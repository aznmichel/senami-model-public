# Book Model (Model: SenamiModel) - VueJS

This is a Book Model of @SenamiModel developed in VueJS. 

The template used comes from the [Creative Tim - Vue Material](https://www.creative-tim.com/vuematerial/)

## Screenshots

DEMO link available [here](https://senami-model.netlify.app/).

|                                               Landing Screen 1                                                |                                                 Landing Screen 2                                                 |                                                                                              Landing Screen 3                                                 |                                                                                                Profile Screen                                                   |
| :---------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------: |
| ![Landing Screen 1](docs/landing1-screen.png?raw=true "Landing 1") | ![Landing Screen 2](docs/landing2-screen.png?raw=true "Landing 2") | ![Landing Screen 3](docs/landing3-screen.png?raw=true "Landing 3") | ![Profile Screen](docs/profile-screen.png?raw=true "Profile") |

## Getting Started

### Prerequisites

- To run this project, you must have [npm](https://www.npmjs.com/) and [vue-cli](https://github.com/vuejs/vue-cli) (optionnal) installed in your machine.

### Project setup
After cloning the repository to your machine:
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
