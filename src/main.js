import Vue from "vue"
import App from "./App.vue"
import router from "./router"

import MaterialKit from "./plugins/material-kit"

import VueSilentbox from "vue-silentbox"

Vue.config.productionTip = false

Vue.use(MaterialKit)
Vue.use(VueSilentbox)

const NavbarStore = {
  showNavbar: false
}

Vue.mixin({
  data() {
    return {
      NavbarStore
    }
  }
})

new Vue({
  router,
  render: h => h(App)
}).$mount("#app")
